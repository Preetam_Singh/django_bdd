Feature: Testing login feature

    Scenario Outline: Login with valid credentials
        Given users <username> and <password>
        Then You should logged in as <role>

        Examples:
            | username | password   | role   |
            | abc      | passwd1234 | Admin  |
            | pqr      | passwd1234 | Staff  |
            | xyz      | passwd1234 | Normal |

    Scenario Outline: Login with invalid credentials
        Given users <username> and <password>
        Then Invalid username or password

        Examples:
            | username | password |
            | abc      | passwd   |
            | pqr      | passwd   |
            | xyz      | passwd   |