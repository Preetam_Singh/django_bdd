from behave import *
from django.test import Client
from django.contrib.auth.models import User


@given(u'users {username} and {password}')
def enter_credentials(context, username, password):
    context.response = context.test.client.login(
        username=username, password=password)
    if context.response:
        context.user = User.objects.get(username=username)


@ then(u'You should logged in as {role}')
def check_message(context, role):
    if role == 'Admin':
        assert context.user.is_superuser
    elif role == 'Staff':
        assert context.user.is_staff
    elif role == 'Normal':
        assert context.user.is_active
    else:
        assert False


@ then(u'Invalid username or password')
def check_invalid_message(context):
    assert context.response == False
