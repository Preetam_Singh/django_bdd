from behave import *
from selenium import webdriver


@given(u'Login page is loaded')
def load_login(context):
    context.browser.get(context.root_url + '/admin')


@given(u'user enters {username} and {password}')
def enter_credentials(context, username, password):
    context.browser.find_element_by_name('username').send_keys(username)
    context.browser.find_element_by_name('password').send_keys(password)


@when(u'user click login button')
def click_button(context):
    context.browser.find_element_by_xpath("//input[@type='submit']").click()


@then(u'You should see the login page')
def check_message(context):
    message = context.browser.find_element_by_xpath(
        '/html/body/div/div[1]/div[1]/h1/a').text
    user_msg = 'Django administration'
    assert message == user_msg


@then(u'Invalid username or password should be displayed as message')
def check_invalid_message(context):
    message = context.browser.find_element_by_class_name('errornote').text
    assert message != ''
