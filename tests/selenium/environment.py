from selenium import webdriver


def before_all(context):
    context.root_url = "http://127.0.0.1:8000"


def before_feature(context, feature):
    context.browser = webdriver.Firefox()


def after_scenario(context, scenario):
    context.browser.get(context.root_url + '/admin/logout/')


def after_feature(context, feature):
    context.browser.close()
