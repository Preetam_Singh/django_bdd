Feature: Testing login feature

    Scenario Outline: Login with valid credentials
        Given Login page is loaded
        And user enters <username> and <password>
        When user click login button
        Then You should see the login page

        Examples:
            | username | password   |
            | abc      | passwd1234 |
            | pqr      | passwd1234 |


    Scenario Outline: Login with invalid credentials
        Given Login page is loaded
        And user enters <username> and <password>
        When user click login button
        Then Invalid username or password should be displayed as message

        Examples:
            | username | password   |
            | abc      | passwd     |
            | pqr      | passwd     |
            | xyz      | passwd1234 |