### Run server 
* `python manage.py runserver`

### To create users(Management command)
* `python manage.py create_users`

### To Run selenium Tests

* Uncomment `paths = tests/selenium` in behave.ini file
* run `python manage.py behave`


### To Run Django Tests
* Uncomment `paths = tests/simple` in behave.ini file
* run `python manage.py behave --use-existing-database`