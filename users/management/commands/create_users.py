from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Creating Test Users'

    def handle(self, *args, **options):
        self.__class__.create_new_user(
            'abc', 'abc@gmail.com', 'passwd1234', is_admin=True, is_staff=True)
        self.__class__.create_new_user(
            'pqr', 'pqr@gmail.com', 'passwd1234', is_admin=False, is_staff=True)
        self.__class__.create_new_user(
            'xyz', 'xyz@gmail.com', 'passwd1234', is_admin=False, is_staff=False)

        self.stdout.write(self.style.SUCCESS('Successfully created users'))

    @staticmethod
    def create_new_user(username, email, password, is_admin, is_staff):
        user = User.objects.create_user(username, email)
        user.set_password(password)
        user.is_admin = is_admin
        if is_admin:
            user.is_superuser = True
        user.is_staff = is_staff
        user.save()
