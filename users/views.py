from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib import messages

from .forms import LoginForm


def home(request):

    context = {
        'is_logged_in': False,
    }

    if request.user.is_authenticated:
        logged_in_user = User.objects.get(username=request.user.username)
        context.update(
            {
                'is_logged_in': True,
                'user': logged_in_user
            }
        )

        if logged_in_user.is_superuser:
            messages.info(request, 'You are logged in as Admin user')
        elif logged_in_user.is_staff:
            messages.info(request, 'You are logged in as Staff user')
        else:
            messages.info(request, 'You are logged in as Normal user')

    return render(request, 'home.html', context=context)


def login_page(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                messages.info(request, 'Invalid username or password')
                return render(request, 'login.html', {'form': form})
        else:
            return render(request, 'login.html', {'form': form})
    else:
        form = LoginForm()
        return render(request, 'login.html', {'form': form})
